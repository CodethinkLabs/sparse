#ifndef VERIFY_FORMAT_H
#define VERIFY_FORMAT_H

void verify_format_attribute(struct symbol *fn, struct expression_list *args);

#endif
